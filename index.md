---
title: "Accueil"
order: 0
in_menu: true
---
# Mon mini-site Framalibre

![Texte décrivant l'image](/images/Capture d’écran 2023-12-05 à 10.39.48.png)

J'écris ici un texte de présentation de mon mini-site Framalibre et je peux
lister les logiciels libres que je recommande.

<article class="framalibre-notice">
  <div>
    <img src="https://beta.framalibre.org/images/logo/PeerTube.png">
  </div>
  <div>
    <h2>PeerTube</h2>
    <p>PeerTube est un logiciel décentralisé et fédéré d'hébergement de vidéos.</p>
    <div>
      <a href="https://beta.framalibre.org/notices/peertube.html">Vers la notice Framalibre</a>
      <a href="https://joinpeertube.org/fr/">Vers le site</a>
    </div>
  </div>
</article> 